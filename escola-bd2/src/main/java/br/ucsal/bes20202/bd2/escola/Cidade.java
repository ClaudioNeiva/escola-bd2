package br.ucsal.bes20202.bd2.escola;

import java.util.Objects;

public class Cidade {

	// char(3)
	private String sigla;

	// varchar(40)
	private String nome;

	private Uf uf;

	public Cidade() {
	}

	public Cidade(Uf uf, String sigla, String nome) {
		super();
		this.uf = uf;
		this.sigla = sigla;
		this.nome = nome;
	}

	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		return Objects.hash(nome, sigla, uf);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cidade other = (Cidade) obj;
		return Objects.equals(nome, other.nome) && Objects.equals(sigla, other.sigla) && Objects.equals(uf, other.uf);
	}

	@Override
	public String toString() {
		return "Cidade [sigla=" + sigla + ", nome=" + nome + ", uf=" + uf + "]";
	}

}