package br.ucsal.bes20202.bd2.escola;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Aluno {

	// int
	private Integer matricula;

	// varchar(40)
	private String nome;

	// char(11)
	private String cpf;

	private List<Curso> cursos;

	// char(3)
	private SituacaoAlunoEnum situacao;

	// numeric(10,2)
	private BigDecimal rendaFamiliar;

	private Long numRg;

	private String orgaoExpedidor;

	private Uf ufOrgaoExpedidor;

	// cada telefone deve ser varchar(15)
	private List<String> telefones;

	private Endereco endereco;

	public Aluno() {
		super();
	}

	public Aluno(String nome, SituacaoAlunoEnum situacaoAlunoEnum) {
		this.nome = nome;
		this.situacao = situacaoAlunoEnum;
	}

	public Aluno(String nome, Curso curso, SituacaoAlunoEnum situacao) {
		this(nome, situacao);
		this.cursos = Arrays.asList(curso);
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCurso(List<Curso> cursos) {
		this.cursos.clear();
		this.cursos.addAll(cursos);
	}

	public SituacaoAlunoEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoAlunoEnum situacao) {
		this.situacao = situacao;
	}

	public BigDecimal getRendaFamiliar() {
		return rendaFamiliar;
	}

	public void setRendaFamiliar(BigDecimal rendaFamiliar) {
		this.rendaFamiliar = rendaFamiliar;
	}

	public Long getNumRg() {
		return numRg;
	}

	public void setNumRg(Long numRg) {
		this.numRg = numRg;
	}

	public String getOrgaoExpedidor() {
		return orgaoExpedidor;
	}

	public void setOrgaoExpedidor(String orgaoExpedidor) {
		this.orgaoExpedidor = orgaoExpedidor;
	}

	public Uf getUfOrgaoExpedidor() {
		return ufOrgaoExpedidor;
	}

	public void setUfOrgaoExpedidor(Uf ufOrgaoExpedidor) {
		this.ufOrgaoExpedidor = ufOrgaoExpedidor;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public int hashCode() {
		return Objects.hash(cpf, cursos, endereco, matricula, nome, numRg, orgaoExpedidor, rendaFamiliar, situacao,
				telefones, ufOrgaoExpedidor);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		return Objects.equals(cpf, other.cpf) && Objects.equals(cursos, other.cursos)
				&& Objects.equals(endereco, other.endereco) && Objects.equals(matricula, other.matricula)
				&& Objects.equals(nome, other.nome) && Objects.equals(numRg, other.numRg)
				&& Objects.equals(orgaoExpedidor, other.orgaoExpedidor)
				&& Objects.equals(rendaFamiliar, other.rendaFamiliar) && situacao == other.situacao
				&& Objects.equals(telefones, other.telefones)
				&& Objects.equals(ufOrgaoExpedidor, other.ufOrgaoExpedidor);
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + ", cpf=" + cpf + ", cursos=" + cursos
				+ ", situacao=" + situacao + ", rendaFamiliar=" + rendaFamiliar + ", numRg=" + numRg
				+ ", orgaoExpedidor=" + orgaoExpedidor + ", ufOrgaoExpedidor=" + ufOrgaoExpedidor + ", telefones="
				+ telefones + ", endereco=" + endereco + "]";
	}

	
}
